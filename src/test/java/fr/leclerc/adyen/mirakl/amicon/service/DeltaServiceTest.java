package fr.leclerc.adyen.mirakl.amicon.service;

import fr.leclerc.adyen.mirakl.amicon.AmiconApplication;
import fr.leclerc.adyen.mirakl.amicon.config.ApplicationProperties;
import fr.leclerc.adyen.mirakl.amicon.domain.MiraklDelta;
import fr.leclerc.adyen.mirakl.amicon.repository.MiraklDeltaRepository;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import com.google.common.collect.ImmutableList;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = AmiconApplication.class)
@Transactional
public class DeltaServiceTest {
    @Autowired
    private DeltaService deltaService;

    @Autowired
    private MiraklDeltaRepository miraklDeltaRepository;

    @Autowired
    private ApplicationProperties applicationProperties;

    private Date date = new Date(0);

    @BeforeEach
    public void removeExistingTestDelta() {
        final List<MiraklDelta> all = miraklDeltaRepository.findAll();
        miraklDeltaRepository.deleteAll(all);
        miraklDeltaRepository.flush();
        applicationProperties.setInitialDeltaDaysBack(0);
    }

    @Test
    public void onlyGetsTheLatestDeltaEvenThoughMoreThanShouldNotExist() {
        final MiraklDelta miraklDelta1 = new MiraklDelta();
        miraklDelta1.setShopDelta(ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).plusDays(1));

        final MiraklDelta miraklDelta2 = new MiraklDelta();
        miraklDelta2.setShopDelta(ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).plusDays(2));

        final MiraklDelta miraklDelta3 = new MiraklDelta();
        miraklDelta3.setShopDelta(ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()));

        miraklDeltaRepository.saveAll(ImmutableList.of(miraklDelta1, miraklDelta2, miraklDelta3));
        miraklDeltaRepository.flush();

        final Date result = deltaService.getShopDelta();
        Assertions.assertThat(result).hasSameTimeAs(date);
    }

    @Test
    public void noCurrentDeltaReturnsNow() {
        final Date now = new Date();
        final Date result = deltaService.getShopDelta();
        Assertions.assertThat(result).isNotNull();
        Assertions.assertThat(result).isInSameSecondWindowAs(now);
    }

    @Test
    public void createNewDeltaWithExisting() {
        final MiraklDelta miraklDelta1 = new MiraklDelta();
        miraklDelta1.setShopDelta(ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).plusDays(1));
        final Long id = miraklDeltaRepository.saveAndFlush(miraklDelta1).getId();

        final ZonedDateTime now = ZonedDateTime.now();
        deltaService.updateShopDelta(now);

        final List<MiraklDelta> all = miraklDeltaRepository.findAll();
        Assertions.assertThat(all.size()).isEqualTo(1);
        final MiraklDelta miraklDelta = all.iterator().next();
        Assertions.assertThat(miraklDelta.getId()).isEqualTo(id);
        Assertions.assertThat(miraklDelta.getShopDelta()).isEqualTo(now);
    }

    @Test
    public void createDeltaWhenNonAlreadyExist() {
        deltaService.getShopDelta();

        final List<MiraklDelta> all = miraklDeltaRepository.findAll();
        final Date now = new Date();
        Assertions.assertThat(all.size()).isEqualTo(1);
        Assertions.assertThat(Date.from(all.get(0).getShopDelta().toInstant())).isInSameSecondWindowAs(now);
    }    
}