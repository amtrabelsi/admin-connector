package fr.leclerc.adyen.mirakl.amicon.service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class ShopService {

    private static final Logger LOGGER = LogManager.getLogger(ShopService.class);

    @Resource
    private DeltaService deltaService;

    @Scheduled(fixedRate = 5000, initialDelay=5000)
    public void processUpdatedShops() {
        LOGGER.info("--- Processing updated shops ---");

        final ZonedDateTime beforeProcessing = ZonedDateTime.now();
        List<Object> shops = getUpdatedShops();
        
        System.out.println("pulling shops...");
        deltaService.updateShopDelta(beforeProcessing);
    }

    public List<Object> getUpdatedShops() {
        int offset = 0;
        Long totalCount = 1L;
        List<Object> shops = new ArrayList<>();
        deltaService.getShopDelta();

        /*while (offset < totalCount) {
            MiraklGetShopsRequest miraklGetShopsRequest = new MiraklGetShopsRequest();
            miraklGetShopsRequest.setOffset(offset);

            miraklGetShopsRequest.setUpdatedSince(deltaService.getShopDelta());
            log.debug("getShops request since: " + miraklGetShopsRequest.getUpdatedSince());
            MiraklShops miraklShops = miraklMarketplacePlatformOperatorApiClient.getShops(miraklGetShopsRequest);
            shops.addAll(miraklShops.getShops());

            totalCount = miraklShops.getTotalCount();
            offset += miraklShops.getShops().size();
        }*/

        return shops;
    }
}