package fr.leclerc.adyen.mirakl.amicon.service;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.stereotype.Service;

import fr.leclerc.adyen.mirakl.amicon.config.ApplicationProperties;
import fr.leclerc.adyen.mirakl.amicon.domain.MiraklDelta;
import fr.leclerc.adyen.mirakl.amicon.domain.MiraklDocumentDelta;
import fr.leclerc.adyen.mirakl.amicon.repository.MiraklDeltaRepository;
import fr.leclerc.adyen.mirakl.amicon.repository.MiraklDocumentDeltaRepository;

@Service
public class DeltaService {
    private static final Logger LOGGER = LogManager.getLogger(DeltaService.class);

    @Resource
    private ApplicationProperties applicationProperties;

    @Resource
    private MiraklDeltaRepository miraklDeltaRepository;

    @Resource
    private MiraklDocumentDeltaRepository miraklDocumentDeltaRepository;

    /**
     * Get shop delta If doens't exist, create and return a new one using
     * application.initialDeltaDaysBack property
     */
    public Date getShopDelta() {
        final Optional<MiraklDelta> firstByOrderByIdDesc = miraklDeltaRepository.findFirstByOrderByIdDesc();

        if (firstByOrderByIdDesc.isPresent()) {
            return Date.from(firstByOrderByIdDesc.get().getShopDelta().toInstant());
        }

        LOGGER.info("No shopDelta found");
        ZonedDateTime defaultDate = ZonedDateTime.now().minusDays(applicationProperties.getInitialDeltaDaysBack());
        createNewShopDelta(defaultDate);
        return Date.from(defaultDate.toInstant());
    }

    private void createNewShopDelta(ZonedDateTime delta) {
        LOGGER.debug("Creating new shopDelta");
        final MiraklDelta miraklDelta = new MiraklDelta();
        miraklDelta.setShopDelta(delta);
        miraklDeltaRepository.saveAndFlush(miraklDelta);
    }

    public void updateShopDelta(ZonedDateTime delta) {
        MiraklDelta entity = miraklDeltaRepository.findFirstByOrderByIdDesc()
                .orElseThrow(() -> new IllegalStateException("No shopDelta found"));
        entity.setShopDelta(delta);
        miraklDeltaRepository.saveAndFlush(entity);
    }

    /**
     * Get document delta If doens't exist, create and return a new one using
     * application.initialDeltaDaysBack property
     */
    public Date getDocumentDelta() {
        final Optional<MiraklDocumentDelta> firstByOrderByIdDesc = miraklDocumentDeltaRepository
                .findFirstByOrderByIdDesc();

        if (firstByOrderByIdDesc.isPresent()) {
            return Date.from(firstByOrderByIdDesc.get().getDocumentDelta().toInstant());
        }

        LOGGER.info("No documentDelta found");
        ZonedDateTime defaultDate = ZonedDateTime.now().minusDays(applicationProperties.getInitialDeltaDaysBack());
        createNewDocumentDelta(defaultDate);
        return Date.from(defaultDate.toInstant());
    }

    private void createNewDocumentDelta(ZonedDateTime delta) {
        LOGGER.debug("Creating new documentDelta");
        final MiraklDocumentDelta miraklDelta = new MiraklDocumentDelta();
        miraklDelta.setDocumentDelta(delta);
        miraklDocumentDeltaRepository.saveAndFlush(miraklDelta);
    }

    public void updateDocumentDelta(ZonedDateTime delta) {
        MiraklDocumentDelta entity = miraklDocumentDeltaRepository.findFirstByOrderByIdDesc()
                .orElseThrow(() -> new IllegalStateException("No DocumentDelta found"));
        entity.setDocumentDelta(delta);
        miraklDocumentDeltaRepository.saveAndFlush(entity);
    }
}