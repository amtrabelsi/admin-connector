package fr.leclerc.adyen.mirakl.amicon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.leclerc.adyen.mirakl.amicon.config.ApplicationProperties;
import fr.leclerc.adyen.mirakl.amicon.config.DefaultProfileUtil;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties(ApplicationProperties.class)
public class AmiconApplication {
	private static final Logger LOGGER = LogManager.getLogger(AmiconApplication.class);

	private final Environment env;

	public AmiconApplication(Environment env) {
        this.env = env;
	}
	
	/**
	 * Initializes adyen Mirakl Connector AmiconApplication.
	 * <p>
	 * Spring profiles can be configured with a program arguments
	 * --spring.profiles.active=your-active-profile
	 * <p>
	 */
	@PostConstruct
	public void initApplication() {
		Collection<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
		if (activeProfiles.contains("dev")
				&& activeProfiles.contains("prod")) {
			LOGGER.error("You have misconfigured your application! It should not run with both the 'dev' and 'prod' profiles at the same time.");
		}
		if (activeProfiles.contains("dev")
				&& activeProfiles.contains("cloud")) {
			LOGGER.error("You have misconfigured your application! It should not run with both the 'dev' and 'cloud' profiles at the same time.");
		}
	}
	public static void main(String[] args) throws UnknownHostException {
		SpringApplication app = new SpringApplication(AmiconApplication.class);
        DefaultProfileUtil.addDefaultProfile(app);
        Environment env = app.run(args).getEnvironment();
        String protocol = "http";
        if (env.getProperty("server.ssl.key-store") != null) {
            protocol = "https";
        }
        LOGGER.info("\n----------------------------------------------------------\n\t" +
                "Application '{}' is running! Access URLs:\n\t" +
                "Local: \t\t{}://localhost:{}\n\t" +
                "External: \t{}://{}:{}\n\t" +
                "Profile(s): \t{}\n----------------------------------------------------------",
            env.getProperty("spring.application.name"),
            protocol,
            env.getProperty("server.port"),
            protocol,
            InetAddress.getLocalHost().getHostAddress(),
            env.getProperty("server.port"),
			env.getActiveProfiles());
			

		//SpringApplication.run(AmiconApplication.class, args);
	}
}
