package fr.leclerc.adyen.mirakl.amicon.repository;

import org.springframework.stereotype.Repository;

import fr.leclerc.adyen.mirakl.amicon.domain.MiraklDelta;

import java.util.Optional;

import org.springframework.data.jpa.repository.*;

@Repository
public interface MiraklDeltaRepository extends JpaRepository<MiraklDelta, Long> {
    Optional<MiraklDelta> findFirstByOrderByIdDesc();
}