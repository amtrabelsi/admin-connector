package fr.leclerc.adyen.mirakl.amicon.config;

import java.util.Map;
import java.util.regex.Pattern;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.google.common.collect.ImmutableMap;

/**
 * Properties specific to Adyen Mirakl Connector.
 * <p>
 * Properties are configured in the application.yml file.
 */
@Configuration
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
    private String miraklPullCron;
    private String payoutRetryCron;
    private String retryDocsCron;
    private Integer initialDeltaDaysBack;
    private Integer maxPayoutFailed;
    private Integer maxDocRetries;
    private Map<String, String> houseNumbersRegex;
    // private String basicUsername;
    // private String basicPassword;
    //private Integer defaultProcessingTier;
    
    private final Cache cache = new Cache();

    @Bean
    public Map<String, Pattern> houseNumberPatterns() {
        final ImmutableMap.Builder<String, Pattern> builder = ImmutableMap.builder();
        houseNumbersRegex.forEach((k, v) -> builder.put(k, Pattern.compile(v)));
        return builder.build();
    }

    public String getMiraklPullCron() {
        return miraklPullCron;
    }

    public void setMiraklPullCron(final String miraklPullCron) {
        this.miraklPullCron = miraklPullCron;
    }

    public String getPayoutRetryCron() {
        return payoutRetryCron;
    }

    public void setPayoutRetryCron(String payoutRetryCron) {
        this.payoutRetryCron = payoutRetryCron;
    }

    public Integer getInitialDeltaDaysBack() {
        return initialDeltaDaysBack;
    }

    public void setInitialDeltaDaysBack(Integer initialDeltaDaysBack) {
        this.initialDeltaDaysBack = initialDeltaDaysBack;
    }

    public Integer getMaxPayoutFailed() {
        return maxPayoutFailed;
    }

    public void setMaxPayoutFailed(Integer maxPayoutFailed) {
        this.maxPayoutFailed = maxPayoutFailed;
    }

    public Integer getMaxDocRetries() {
        return maxDocRetries;
    }

    public void setMaxDocRetries(Integer maxDocRetries) {
        this.maxDocRetries = maxDocRetries;
    }

    public Map<String, String> getHouseNumbersRegex() {
        return houseNumbersRegex;
    }

    public void setHouseNumbersRegex(final Map<String, String> houseNumbersRegex) {
        this.houseNumbersRegex = houseNumbersRegex;
    }

    // public String getBasicUsername() {
    //     return basicUsername;
    // }

    // public void setBasicUsername(String basicUsername) {
    //     this.basicUsername = basicUsername;
    // }

    // public String getBasicPassword() {
    //     return basicPassword;
    // }

    // public void setBasicPassword(String basicPassword) {
    //     this.basicPassword = basicPassword;
    // }

    public String getRetryDocsCron() {
        return retryDocsCron;
    }

    public void setRetryDocsCron(final String retryDocsCron) {
        this.retryDocsCron = retryDocsCron;
    }

    // public Integer getDefaultProcessingTier() {
    //     return defaultProcessingTier;
    // }

    // public void setDefaultProcessingTier(final Integer defaultProcessingTier) {
    //     this.defaultProcessingTier = defaultProcessingTier;
    // }

    public static class Cache {

        private final Ehcache ehcache = new Ehcache();

        public Ehcache getEhcache() {
            return ehcache;
        }

        public static class Ehcache {

            private int timeToLiveSeconds = 3600;

            private long maxEntries = 100;

            public int getTimeToLiveSeconds() {
                return timeToLiveSeconds;
            }

            public void setTimeToLiveSeconds(int timeToLiveSeconds) {
                this.timeToLiveSeconds = timeToLiveSeconds;
            }

            public long getMaxEntries() {
                return maxEntries;
            }

            public void setMaxEntries(long maxEntries) {
                this.maxEntries = maxEntries;
            }
        }
    }

    /**
     * <p>
     * Getter for the field <code>cache</code>.
     * </p>
     *
     * @return an ApplicationProperties.Cache object.
     */
    public Cache getCache() {
        return cache;
    }
}