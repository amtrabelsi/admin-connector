package fr.leclerc.adyen.mirakl.amicon.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import fr.leclerc.adyen.mirakl.amicon.domain.MiraklDocumentDelta;

/**
 * Spring Data JPA repository for the MiraklDocumentDelta entity.
 */
@Repository
public interface MiraklDocumentDeltaRepository extends JpaRepository<MiraklDocumentDelta, Long> {

    Optional<MiraklDocumentDelta> findFirstByOrderByIdDesc();
}