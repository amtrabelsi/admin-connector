package fr.leclerc.adyen.mirakl.amicon.config;

import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.jsr107.Eh107Configuration;

import java.time.Duration;

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
@AutoConfigureBefore(value = { DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(ApplicationProperties applicationProperties) {
        ApplicationProperties.Cache.Ehcache ehcache = applicationProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(CacheConfigurationBuilder
                .newCacheConfigurationBuilder(Object.class, Object.class,
                        ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(
                        ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            //cm.createCache(com.adyen.mirakl.domain.AdyenNotification.class.getName(), jcacheConfiguration);
            cm.createCache(fr.leclerc.adyen.mirakl.amicon.domain.MiraklDelta.class.getName(), jcacheConfiguration);
            cm.createCache(fr.leclerc.adyen.mirakl.amicon.domain.MiraklDocumentDelta.class.getName(), jcacheConfiguration);
            //cm.createCache(com.adyen.mirakl.domain.ProcessEmail.class.getName(), jcacheConfiguration);
            //cm.createCache(com.adyen.mirakl.domain.EmailError.class.getName(), jcacheConfiguration);
            //cm.createCache(com.adyen.mirakl.domain.ProcessEmail.class.getName() + ".emailErrors", jcacheConfiguration);
            //cm.createCache(com.adyen.mirakl.domain.ShareholderMapping.class.getName(), jcacheConfiguration);
            //cm.createCache(com.adyen.mirakl.domain.AdyenPayoutError.class.getName(), jcacheConfiguration);
            //cm.createCache(com.adyen.mirakl.domain.MiraklVoucherEntry.class.getName(), jcacheConfiguration);
            //cm.createCache(com.adyen.mirakl.domain.DocRetry.class.getName(), jcacheConfiguration);
            //cm.createCache(com.adyen.mirakl.domain.DocError.class.getName(), jcacheConfiguration);
            //cm.createCache(com.adyen.mirakl.domain.DocRetry.class.getName() + ".docErrors", jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}